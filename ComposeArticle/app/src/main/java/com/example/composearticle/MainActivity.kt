package com.example.composearticle

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.composearticle.ui.theme.ComposeArticleTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeArticleTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Article(header = stringResource(id = R.string.article_header), intro = stringResource(id = R.string.article_intro), body = stringResource(
                        id = R.string.article_body
                    ))
                }
            }
        }
    }
}

@Composable
fun Article(header: String, intro: String, body: String, modifier: Modifier = Modifier) {
    val image = painterResource(id = R.drawable.bg_compose_background)
    Column(modifier = modifier) {
        Image(painter = image, contentDescription = stringResource(R.string.article_header_image))
        ArticleText(header = header, intro = intro, body = body)
    }
}

@Composable
fun ArticleHeader(text: String, modifier: Modifier = Modifier) {
    Text(
        text = text,
        fontSize = 24.sp,
        modifier = modifier.padding(16.dp)
    )
}

@Composable
fun ArticleIntro(text: String, modifier: Modifier = Modifier) {
    Text(
        text = text,
        modifier = modifier.padding(start = 16.dp, end = 16.dp),
        textAlign = TextAlign.Justify
    )
}

@Composable
fun ArticleBody(text: String, modifier: Modifier = Modifier) {
    Text(
        text = text,
        modifier = modifier.padding(16.dp),
        textAlign = TextAlign.Justify
    )
}

@Composable
fun ArticleText(header: String, intro: String, body: String, modifier: Modifier = Modifier) {

    Column(modifier = modifier) {
        ArticleHeader(text = header)
        ArticleIntro(text = intro)
        ArticleBody(text = body)
    }
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    ComposeArticleTheme {

        Article(header = stringResource(id = R.string.article_header), intro = stringResource(id = R.string.article_intro), body = stringResource(
            id = R.string.article_body
        ))
        //ArticleText(header = stringResource(id = R.string.article_header), intro = stringResource(id = R.string.article_intro), body = stringResource( id = R.string.article_body ))
        //ArticleImage()
        //ArticleHeader(text = stringResource(R.string.article_header))
        //ArticleIntro(text = stringResource(R.string.article_intro))
        //ArticleBody(text = stringResource(id = R.string.article_body))
    }
}