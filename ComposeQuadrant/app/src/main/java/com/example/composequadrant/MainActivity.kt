package com.example.composequadrant

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.composequadrant.ui.theme.ComposeQuadrantTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ComposeQuadrantTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    ComposeQuadrant(
                        title1 = stringResource(id = R.string.text_composable_title), body1 = stringResource(id = R.string.text_composable_body), color1 = colorResource(
                            id = R.color.first_quarter
                        ), title2 = stringResource(id = R.string.image_composable_title), body2 = stringResource(id = R.string.image_composable_body), color2 = colorResource(
                            id = R.color.second_quarter
                        ), title3 = stringResource(id = R.string.row_composable_title), body3 = stringResource(id = R.string.row_composable_body), color3 = colorResource(
                            id = R.color.third_quarter
                        ), title4 = stringResource(id = R.string.column_composable_title), body4 = stringResource(id = R.string.column_composable_body), color4 = colorResource(
                            id = R.color.fourth_quarter
                        )
                    )
                }
            }
        }
    }
}

@Composable
fun Quadrant(title: String, body: String, modifier: Modifier = Modifier) {

    Column(
        modifier = modifier
            .padding(16.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,

    ) {
        Text(
            text = title,
            fontWeight = FontWeight.Bold,
            modifier = modifier.padding(bottom = 16.dp)
        )
        Text(
            text = body,
            textAlign = TextAlign.Justify
        )
    }

}

/* My Solution */
@Composable
fun ComposeQuadrant(
    title1: String, body1: String, color1: Color,
    title2: String, body2: String, color2: Color,
    title3: String, body3: String, color3: Color,
    title4: String, body4: String, color4: Color,
    modifier: Modifier = Modifier) {
    Column() {
        Row(modifier = modifier.fillMaxHeight(0.5f)) {
            Surface(
                color = color1,
                modifier = modifier
                    .fillMaxWidth(0.5f)
                    .fillMaxHeight()
            ) {
                Quadrant(
                    title = title1,
                    body = body1
                )
            }
            Surface(
                color = color2,
                modifier = modifier
                    .fillMaxWidth()
                    .fillMaxHeight()
            ) {
                Quadrant(
                    title = title2,
                    body = body2
                )
            }
        }
        Row(modifier = modifier.fillMaxHeight()) {
            Surface(
                color = color3,
                modifier = modifier
                    .fillMaxHeight()
                    .fillMaxWidth(0.5f)
            ) {
                Quadrant(
                    title = title3,
                    body = body3
                )
            }
            Surface(
                color = color4,
                modifier = modifier.fillMaxHeight()
            ) {
                Quadrant(
                    title = title4,
                    body = body4
                )
            }
        }
    }
}

/* using weight property */
/* color can be passed as using Color() and passing hex color as argument values and will be applied using background property of modifier.
* */
@Composable
private fun ComposableInfoCard(
    title: String,
    description: String,
    backgroundColor: Color,
    modifier: Modifier = Modifier
) {
    Column(
        modifier = modifier
            .fillMaxSize()
            .background(backgroundColor)
            .padding(16.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = title,
            modifier = Modifier.padding(bottom = 16.dp),
            fontWeight = FontWeight.Bold
        )
        Text(
            text = description,
            textAlign = TextAlign.Justify
        )
    }
}

@Composable
fun ComposeQuadrantApp() {
    Column(Modifier.fillMaxWidth()) {
        Row(Modifier.weight(1f)) {
            ComposableInfoCard(
                title = stringResource(R.string.text_composable_title),
                description = stringResource(R.string.text_composable_body),
                backgroundColor = Color(0xFFEADDFF),
                modifier = Modifier.weight(1f)
            )
            ComposableInfoCard(
                title = stringResource(R.string.image_composable_title),
                description = stringResource(R.string.image_composable_body),
                backgroundColor = Color(0xFFD0BCFF),
                modifier = Modifier.weight(1f)
            )
        }
        Row(Modifier.weight(1f)) {
            ComposableInfoCard(
                title = stringResource(R.string.column_composable_title),
                description = stringResource(R.string.column_composable_body),
                backgroundColor = Color(0xFFB69DF8),
                modifier = Modifier.weight(1f)
            )
            ComposableInfoCard(
                title = stringResource(R.string.row_composable_title),
                description = stringResource(R.string.row_composable_body),
                backgroundColor = Color(0xFFF6EDFF),
                modifier = Modifier.weight(1f)
            )
        }
    }
}


@Preview(showBackground = true)
@Composable
fun GreetingPreview(modifier: Modifier = Modifier) {
    ComposeQuadrantTheme {
//        ComposeQuadrant(
//            title1 = stringResource(id = R.string.text_composable_title), body1 = stringResource(id = R.string.text_composable_body), color1 = colorResource(
//                id = R.color.first_quarter
//            ), title2 = stringResource(id = R.string.image_composable_title), body2 = stringResource(id = R.string.image_composable_body), color2 = colorResource(
//                id = R.color.second_quarter
//            ), title3 = stringResource(id = R.string.row_composable_title), body3 = stringResource(id = R.string.row_composable_body), color3 = colorResource(
//                id = R.color.third_quarter
//            ), title4 = stringResource(id = R.string.column_composable_title), body4 = stringResource(id = R.string.column_composable_body), color4 = colorResource(
//                id = R.color.fourth_quarter
//            )
//        )
        ComposeQuadrantApp()
    }
}
